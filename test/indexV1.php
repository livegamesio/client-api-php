<?php
require_once("../src/Api.php");

$userModel = [
	'id' => "abc1234",  //zorunlu
	'parent' => 'ResellerAccountUserId', //zorunlu
	'name' => "Name", //zorunlu (görünecek isim)
	'surname' => "Surname", //Opsiyonel
	'phone' => "05555555555", //Opsiyonel
	'email' => "user@mail.com", //Opsiyonel - LoginBased sistemler için
	'password' => "123456" //Opsiyonel - LoginBased sistemler için
];
$api = new \LiveGames\ClientApi\Api($userModel, "API_KEY", "API_SECRET");

echo "Token: ---------------------------------------------";
echo $api->token;

echo "Kullanıcı Kontrolü: ---------------------------------------------";
//print_r($api->checkUser());

echo "Kullanıcı Yaratır: ---------------------------------------------";
//print_r($api->createUser());

echo "Cüzdan Bilgisi: ---------------------------------------------";
//print_r($api->getWallet());

echo "Para yatırır: ---------------------------------------------";
//print_r($api->deposit(1000000));

echo "Para Çeker: ---------------------------------------------";
//print_r($api->withdraw(100000));


echo "Odanın Jackpot Bilgisi: ---------------------------------------------";
//print_r($api->jackpot('CLIENT_ROOM_ID'));


echo "Son Kazananlar: ---------------------------------------------";
//print_r($api->lastWinners());


echo "En Çok Kazananlar: ---------------------------------------------";
//print_r($api->mostWinners());


echo "En Çok Kazanan Numaralar: ---------------------------------------------";
//print_r($api->mostWinnerNumbers());


echo "En Çok Çıkan Numaralar: ---------------------------------------------";
//print_r($api->mostDrawnNumbers());


echo "En Çok Kazanan Kartlar: ---------------------------------------------";
//print_r($api->mostWinnerCards());

