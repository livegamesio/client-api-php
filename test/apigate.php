<?php
/*
 * Client tarafına kullanılanımı aşağıaki gibidir.
 */
require_once("../src/Handler.php");

//TODO: POST yerine hardcoded yazılması gerekmektedir.
$apiKey = $_POST && @$_POST['API_KEY'] ? $_POST['API_KEY'] : 'YOUR_KEY'; //TODO!
$apiSecret = $_POST && @$_POST['API_SECRET'] ? $_POST['API_SECRET'] : 'YOUR_SECRET'; //TODO!

//Gelen aksiyonu kendi kodunuzda kullanmak için $_POST["lgAction"] kullanılabilir.
$action = $_POST && @$_POST["lgAction"] ? $_POST["lgAction"] : "Ping"; // mvc yapısınaki controller altındaki action'dır.

//Handler
$liveGamesHandler = new \LiveGames\ClientApi\Handler($apiKey, $apiSecret);

switch ($action) {
	case "GetWallet":

		//-- Vars ----->
		if ($liveGamesHandler->isValidRequest) {

			$usrId = $liveGamesHandler->request->uid; //your user id

			echo $liveGamesHandler->setPayload([
				'credit' => 100 //your user credit here!
			])->getResponse();
		} else {
			//none
			echo $liveGamesHandler->getResponse();
		}
		break;
	case "UpdateWallet":

		//-- Vars ----->
		if ($liveGamesHandler->isValidRequest) {
			$transactionId = $liveGamesHandler->request->id;
			$usrId = $liveGamesHandler->request->uid;
			$sessionId = $liveGamesHandler->request->sid;
			$action = $liveGamesHandler->request->action;
			$type = $liveGamesHandler->request->type;
			$amount = $liveGamesHandler->request->amount;
			//--your logic here
			echo $liveGamesHandler->setPayload([
				'id' => substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10), //ResponseId
				'uid' => $usrId,
				'credit' => 222 //User Last Wallet
			])->getResponse();
		} else { //none
			echo $liveGamesHandler->getResponse();
		}

		break;

	case "Ping":

		echo $liveGamesHandler->setPayload([
			'pong' => true
		])->getResponse();

		break;
}
